api = 2
core = 7.x
projects[drupal][version] = 7.26

; Patches for Core

; Translations.
; This does not work when there is no core translation for the current
; Drupal version... So we still have to use a workaround in our custom
; build script.
