; Drupal.org release file.
core = 7.x
api = 2

; Basic contributed modules.
projects[ctools][version] = 1.4
projects[ctools][subdir] = "contrib"
projects[entity][version] = 1.2
projects[entity][subdir] = "contrib"
projects[entityreference][version] = 1.0
projects[entityreference][subdir] = "contrib"
projects[entity_translation][version] = 1.0-beta3
projects[entity_translation][subdir] = "contrib"
projects[libraries][version] = 2.1
projects[libraries][subdir] = "contrib"
projects[token][version] = 1.5
projects[token][subdir] = "contrib"
projects[views][version] = 3.7
projects[views][subdir] = "contrib"

; Field related modules.
projects[date][version] = 2.6
projects[date][subdir] = "contrib"
projects[link][version] = 1.1
projects[link][subdir] = "contrib"
projects[title][version] = 1.0-alpha7
projects[title][subdir] = "contrib"
projects[title][patch][] = "http://drupal.org/files/title-translation_overwrite-1269076-35.patch"

; Features related modules.
projects[features][version] = 2.0
projects[features][subdir] = "contrib"
projects[features_extra][version] = 1.0-beta1
projects[features_extra][subdir] = "contrib"
projects[strongarm][version] = 2.0
projects[strongarm][subdir] = "contrib"

; UI improvement modules.
projects[admin_menu][version] = 3.0-rc4
projects[admin_menu][subdir] = "contrib"
projects[checklistapi][version] = 1.1
projects[checklistapi][subdir] = "contrib"
projects[masquerade][version] = 1.0-rc7
projects[masquerade][subdir] = "contrib"
projects[module_filter][version] = 1.8
projects[module_filter][subdir] = "contrib"
projects[panels][version] = 3.3
projects[panels][subdir] = "contrib"
projects[pm_existing_pages][version] = 1.4
projects[pm_existing_pages][subdir] = "contrib"

; File system.
projects[file_entity][version] = 2.0-alpha3
projects[file_entity][subdir] = "contrib"

; Internationalization modules.
projects[variable][version] = 2.3
projects[variable][subdir] = "contrib"
projects[i18n][version] = 1.10
projects[i18n][subdir] = "contrib"
projects[l10n_client][version] = 1.3
projects[l10n_client][subdir] = "contrib"
projects[l10n_update][version] = 1.0-beta3
projects[l10n_update][subdir] = "contrib"
projects[i18nviews][version] = 3.x-dev
projects[i18nviews][subdir] = "contrib"

; User experience modules.
projects[colorbox][version] = 2.4
projects[colorbox][subdir] = "contrib"
projects[jquery_update][version] = 2.3
projects[jquery_update][subdir] = "contrib"

; Content editing modules.
projects[linkit][version] = 3.0
projects[linkit][subdir] = "contrib"
projects[linkit_panel_pages][version] = 2.0
projects[linkit_panel_pages][subdir] = "contrib"
projects[wysiwyg][version] = 2.2
projects[wysiwyg][subdir] = "contrib"

; Email related modules.
projects[mailsystem][version] = 2.34
projects[mailsystem][subdir] = "contrib"
projects[mimemail][version] = 1.0-beta1
projects[mimemail][subdir] = "contrib"

; SEO and statistics modules.
projects[google_analytics][version] = 1.4
projects[google_analytics][subdir] = "contrib"
projects[linkchecker][version] = 1.1
projects[linkchecker][subdir] = "contrib"
projects[metatag][version] = 1.0-beta9
projects[metatag][subdir] = "contrib"
projects[pathauto][version] = 1.2
projects[pathauto][subdir] = "contrib"
projects[redirect][version] = 1.0-rc1
projects[redirect][subdir] = "contrib"
projects[seo_checklist][version] = 4.1
projects[seo_checklist][subdir] = "contrib"
projects[site_map][version] = 1.0
projects[site_map][subdir] = "contrib"
projects[xmlsitemap][version] = 2.0-rc2
projects[xmlsitemap][subdir] = "contrib"

; Security and performance modules.
projects[security_review][version] = 1.1
projects[security_review][subdir] = "contrib"
projects[qa_checklist][version] = 1.0
projects[qa_checklist][subdir] = "contrib"
projects[pasc][version] = 1.0-beta1
projects[pasc][subdir] = "contrib"

; Video.
projects[media][version] = 2.0-alpha3
projects[media][subdir] = "contrib"
projects[media_youtube][version] = 2.0-rc4
projects[media_youtube][subdir] = "contrib"

; Responsive.
projects[breakpoints][version] = 1.1
projects[breakpoints][subdir] = "contrib"
projects[picture][version] = 1.2
projects[picture][subdir] = "contrib"

; Slider.
projects[flexslider][version] = 2.0-alpha3
projects[flexslider][subdir] = "contrib"

; Gallery.
projects[galleryformatter][version] = 1.3
projects[galleryformatter][subdir] = "contrib"
projects[lightbox2][version] = 1.0-beta1
projects[lightbox2][subdir] = "contrib"

; RSS.
projects[views_rss][version] = 2.0-rc3
projects[views_rss][subdir] = "contrib"

; Entityforms.
projects[entityform][version] = 2.0-beta2
projects[entityform][subdir] = "contrib"

; Map.
projects[geofield][version] = 2.1
projects[geofield][subdir] = "contrib"
projects[geophp][version] = 1.7
projects[geophp][subdir] = "contrib"
projects[proj4js][version] = 1.2
projects[proj4js][subdir] = "contrib"
projects[openlayers][version] = 2.0-beta9
projects[openlayers][subdir] = "contrib"

; Translitaration.
projects[transliteration][version] = 3.2
projects[transliteration][subdir] = "contrib"

; Themes.
projects[maps_admin][version] = 1.4

; Libraries.
libraries[ckeditor][type] = "libraries"
libraries[ckeditor][download][type] = "file"
libraries[ckeditor][download][url] = "http://download.cksource.com/CKEditor/CKEditor/CKEditor%203.6.6.1/ckeditor_3.6.6.1.zip"
libraries[colorbox][type] = "libraries"
libraries[colorbox][download][type] = "file"
libraries[colorbox][download][url] = "https://github.com/jackmoore/colorbox/archive/master.zip"
libraries[jquery.mousewheel][type] = "libraries"
libraries[jquery.mousewheel][download][type] = "git"
libraries[jquery.mousewheel][download][tag] = 3.1.4
libraries[jquery.mousewheel][download][url] = "https://github.com/brandonaaron/jquery-mousewheel.git"
libraries[flexslider][type] = "libraries"
libraries[flexslider][download][type] = "file"
libraries[flexslider][download][url] = "https://github.com/woothemes/FlexSlider/archive/master.zip"

