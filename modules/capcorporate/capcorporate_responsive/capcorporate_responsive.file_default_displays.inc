<?php
/**
 * @file
 * capcorporate_responsive.file_default_displays.inc
 */

/**
 * Implements hook_file_default_displays().
 */
function capcorporate_responsive_file_default_displays() {
  $export = array();

  $file_display = new stdClass();
  $file_display->api_version = 1;
  $file_display->name = 'image__default__file_field_picture';
  $file_display->weight = 0;
  $file_display->status = TRUE;
  $file_display->settings = array(
    'picture_group' => 'responsive',
    'fallback_image_style' => '',
    'image_link' => '',
    'colorbox' => 'responsive',
  );
  $export['image__default__file_field_picture'] = $file_display;

  $file_display = new stdClass();
  $file_display->api_version = 1;
  $file_display->name = 'image__default__file_picture';
  $file_display->weight = 0;
  $file_display->status = FALSE;
  $file_display->settings = array(
    'picture_group' => 'responsive',
    'fallback_image_style' => '',
    'alt' => '',
    'title' => '',
  );
  $export['image__default__file_picture'] = $file_display;

  return $export;
}
