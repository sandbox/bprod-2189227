<?php
/**
 * @file
 * capcorporate_responsive.default_picture_mapping.inc
 */

/**
 * Implements hook_default_picture_mapping().
 */
function capcorporate_responsive_default_picture_mapping() {
  $export = array();

  $picture_mapping = new stdClass();
  $picture_mapping->disabled = FALSE; /* Edit this to true to make a default picture_mapping disabled initially */
  $picture_mapping->api_version = 1;
  $picture_mapping->machine_name = 'mobile';
  $picture_mapping->breakpoint_group = 'mobile';
  $picture_mapping->mapping = array(
    'custom.user.mobile' => array(
      '1x' => 'thumbnail',
      '2x' => 'thumbnail',
    ),
  );
  $export['mobile'] = $picture_mapping;

  $picture_mapping = new stdClass();
  $picture_mapping->disabled = FALSE; /* Edit this to true to make a default picture_mapping disabled initially */
  $picture_mapping->api_version = 1;
  $picture_mapping->machine_name = 'phablet';
  $picture_mapping->breakpoint_group = 'phablet';
  $picture_mapping->mapping = array(
    'custom.user.phablet' => array(
      '1x' => 'medium',
      '2x' => 'medium',
    ),
  );
  $export['phablet'] = $picture_mapping;

  $picture_mapping = new stdClass();
  $picture_mapping->disabled = FALSE; /* Edit this to true to make a default picture_mapping disabled initially */
  $picture_mapping->api_version = 1;
  $picture_mapping->machine_name = 'responsive';
  $picture_mapping->breakpoint_group = 'responsive';
  $picture_mapping->mapping = array(
    'custom.user.mobile' => array(
      '1x' => 'mobile',
      '2x' => 'mobile_x2',
    ),
    'custom.user.phablet' => array(
      '1x' => 'phablet',
      '2x' => 'phablet_x2',
    ),
    'custom.user.tablet' => array(
      '1x' => 'tablet',
      '2x' => 'tablet_x2',
    ),
    'custom.user.desktop' => array(
      '1x' => 'desktop',
    ),
  );
  $export['responsive'] = $picture_mapping;

  return $export;
}
