<?php
/**
 * @file
 * capcorporate_mailing.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function capcorporate_mailing_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}
