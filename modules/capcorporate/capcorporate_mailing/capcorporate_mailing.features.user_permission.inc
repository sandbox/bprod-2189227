<?php
/**
 * @file
 * capcorporate_mailing.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function capcorporate_mailing_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'edit mimemail user settings'.
  $permissions['edit mimemail user settings'] = array(
    'name' => 'edit mimemail user settings',
    'roles' => array(
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'mimemail',
  );

  // Exported permission: 'send arbitrary files'.
  $permissions['send arbitrary files'] = array(
    'name' => 'send arbitrary files',
    'roles' => array(),
    'module' => 'mimemail',
  );

  return $permissions;
}
