<?php
/**
 * @file
 * capcorporate_slider.features.fe_block_settings.inc
 */

/**
 * Implements hook_default_fe_block_settings().
 */
function capcorporate_slider_default_fe_block_settings() {
  $export = array();

  $export['version'] = '2.0';

  $export['views-slider-block_1'] = array(
    'cache' => -1,
    'custom' => 0,
    'delta' => 'slider-block_1',
    'module' => 'views',
    'node_types' => array(),
    'pages' => '<front>',
    'roles' => array(),
    'themes' => array(
      'bartik' => array(
        'region' => 'content',
        'status' => 1,
        'theme' => 'bartik',
        'weight' => -11,
      ),
      'maps_admin' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'maps_admin',
        'weight' => 0,
      ),
    ),
    'title' => '',
    'visibility' => 1,
  );

  return $export;
}
