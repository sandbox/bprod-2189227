<?php
/**
 * @file
 * capcorporate_masquerade.features.user_role.inc
 */

/**
 * Implements hook_user_default_roles().
 */
function capcorporate_masquerade_user_default_roles() {
  $roles = array();

  // Exported role: masquerade.
  $roles['masquerade'] = array(
    'name' => 'masquerade',
    'weight' => 3,
  );

  return $roles;
}
