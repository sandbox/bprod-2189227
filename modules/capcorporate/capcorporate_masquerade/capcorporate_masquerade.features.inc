<?php
/**
 * @file
 * capcorporate_masquerade.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function capcorporate_masquerade_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}
