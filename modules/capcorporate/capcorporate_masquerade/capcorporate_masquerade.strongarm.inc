<?php
/**
 * @file
 * capcorporate_masquerade.strongarm.inc
 */

/**
 * Implements hook_strongarm().
 */
function capcorporate_masquerade_strongarm() {
  $export = array();

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'masquerade_admin_roles';
  $strongarm->value = array(
    3 => '3',
    1 => 0,
    2 => 0,
    4 => 0,
  );
  $export['masquerade_admin_roles'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'masquerade_quick_switches';
  $strongarm->value = array(
    0 => '1',
  );
  $export['masquerade_quick_switches'] = $strongarm;

  return $export;
}
