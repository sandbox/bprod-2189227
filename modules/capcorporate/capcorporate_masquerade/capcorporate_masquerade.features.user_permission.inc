<?php
/**
 * @file
 * capcorporate_masquerade.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function capcorporate_masquerade_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'access site in maintenance mode'.
  $permissions['access site in maintenance mode'] = array(
    'name' => 'access site in maintenance mode',
    'roles' => array(
      'masquerade' => 'masquerade',
    ),
    'module' => 'system',
  );

  // Exported permission: 'administer masquerade'.
  $permissions['administer masquerade'] = array(
    'name' => 'administer masquerade',
    'roles' => array(
      'masquerade' => 'masquerade',
    ),
    'module' => 'masquerade',
  );

  // Exported permission: 'masquerade as admin'.
  $permissions['masquerade as admin'] = array(
    'name' => 'masquerade as admin',
    'roles' => array(
      'masquerade' => 'masquerade',
    ),
    'module' => 'masquerade',
  );

  // Exported permission: 'masquerade as user'.
  $permissions['masquerade as user'] = array(
    'name' => 'masquerade as user',
    'roles' => array(
      'masquerade' => 'masquerade',
    ),
    'module' => 'masquerade',
  );

  return $permissions;
}
