<?php
/**
 * @file
 * capcorporate_masquerade.features.fe_block_settings.inc
 */

/**
 * Implements hook_default_fe_block_settings().
 */
function capcorporate_masquerade_default_fe_block_settings() {
  $export = array();

  $export['version'] = '2.0';

  $export['masquerade-masquerade'] = array(
    'cache' => -1,
    'custom' => 0,
    'delta' => 'masquerade',
    'module' => 'masquerade',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'bartik' => array(
        'region' => 'footer_firstcolumn',
        'status' => 1,
        'theme' => 'bartik',
        'weight' => 0,
      ),
      'maps_admin' => array(
        'region' => 'sidebar_first',
        'status' => 1,
        'theme' => 'maps_admin',
        'weight' => 0,
      ),
    ),
    'title' => '',
    'visibility' => 0,
  );

  return $export;
}
