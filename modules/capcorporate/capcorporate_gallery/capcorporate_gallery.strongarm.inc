<?php
/**
 * @file
 * capcorporate_gallery.strongarm.inc
 */

/**
 * Implements hook_strongarm().
 */
function capcorporate_gallery_strongarm() {
  $export = array();

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'lightbox2_custom_class_handler';
  $strongarm->value = '0';
  $export['lightbox2_custom_class_handler'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'lightbox2_custom_trigger_classes';
  $strongarm->value = '';
  $export['lightbox2_custom_trigger_classes'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'lightbox2_disable_nested_galleries';
  $strongarm->value = 0;
  $export['lightbox2_disable_nested_galleries'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'lightbox2_gallery2_blocks';
  $strongarm->value = '0';
  $export['lightbox2_gallery2_blocks'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'lightbox2_image_assist_custom';
  $strongarm->value = '0';
  $export['lightbox2_image_assist_custom'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'lightbox2_image_node';
  $strongarm->value = '2';
  $export['lightbox2_image_node'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'lightbox2_inline';
  $strongarm->value = '0';
  $export['lightbox2_inline'] = $strongarm;

  return $export;
}
