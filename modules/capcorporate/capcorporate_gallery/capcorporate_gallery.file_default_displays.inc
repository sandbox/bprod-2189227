<?php
/**
 * @file
 * capcorporate_gallery.file_default_displays.inc
 */

/**
 * Implements hook_file_default_displays().
 */
function capcorporate_gallery_file_default_displays() {
  $export = array();

  $file_display = new stdClass();
  $file_display->api_version = 1;
  $file_display->name = 'image__default__file_field_galleryformatter_default';
  $file_display->weight = 0;
  $file_display->status = TRUE;
  $file_display->settings = array(
    'slide_style' => 'galleryformatter_slide',
    'thumb_style' => 'galleryformatter_thumb',
    'style' => 'Greenarrows',
    'link_to_full' => 1,
    'link_to_full_style' => '0',
    'modal' => 'lightbox2',
    'linking_method' => 'show_full_link',
  );
  $export['image__default__file_field_galleryformatter_default'] = $file_display;

  $file_display = new stdClass();
  $file_display->api_version = 1;
  $file_display->name = 'image__default__lightbox2_iframe';
  $file_display->weight = 50;
  $file_display->status = FALSE;
  $file_display->settings = '';
  $export['image__default__lightbox2_iframe'] = $file_display;

  $file_display = new stdClass();
  $file_display->api_version = 1;
  $file_display->name = 'image__default__lightbox2_image';
  $file_display->weight = 50;
  $file_display->status = FALSE;
  $file_display->settings = '';
  $export['image__default__lightbox2_image'] = $file_display;

  return $export;
}
