<?php
/**
 * @file
 * capcorporate_multilingual.features.menu_links.inc
 */

/**
 * Implements hook_menu_default_menu_links().
 */
function capcorporate_multilingual_menu_default_menu_links() {
  $menu_links = array();

  // Exported menu link: navigation_translate-strings:locale
  $menu_links['navigation_translate-strings:locale'] = array(
    'menu_name' => 'navigation',
    'link_path' => 'locale',
    'router_path' => 'locale',
    'link_title' => 'Translate strings',
    'options' => array(
      'identifier' => 'navigation_translate-strings:locale',
    ),
    'module' => 'system',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 0,
  );
  // Translatables
  // Included for use with string extractors like potx.
  t('Translate strings');


  return $menu_links;
}
