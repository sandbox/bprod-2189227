<?php

/**
 * @file
 * Contains Batch API callbacks used during installation.
 */

/**
 * BatchAPI callback.
 *
 * @see capcorporate_install_additional_modules()
 */
function _capcorporate_enable_theme($theme, &$context) {
  theme_enable(array($theme));
  variable_set('theme_default', $theme);
  $context['message'] = st('Installed the default theme.');
}

/**
 * BatchAPI callback.
 *
 * @see capcorporate_install_additional_modules()
 */
function _capcorporate_enable_module($module, $module_name, &$context) {
  module_enable(array($module), FALSE);
  $context['message'] = st('Installed %module module.', array('%module' => $module_name));
}

/**
 * BatchAPI callback.
 *
 * @see capcorporate_install_additional_modules()
 */
function _capcorporate_setup_localization($operation, &$context) {
  require_once DRUPAL_ROOT . '/includes/language.inc';
  $context['message'] = t('@operation', array('@operation' => $operation));

  // Enable en prefix for english language.
  db_update('languages')
    ->fields(array(
      'prefix' => 'en',
    ))
    ->condition('language',  'en')
    ->execute();

  // Enable language detection via url.
  $negotiation['locale-url'] = array(
    'types' => array(
      'language_content',
      'language',
      'language_url',
    ),
    'callbacks' => array(
      'language' => 'locale_language_from_url',
      'switcher' => 'locale_language_switcher_url',
      'url_rewrite' => 'locale_language_url_rewrite_url',
    ),
    'file' => 'includes/locale.inc',
    'weight' => '-8,',
    'name' => 'URL',
    'description' => t('Determine the language from the URL (Path prefix or domain).'),
    'config' => 'admin/config/regional/language/configure/url',
  );
  language_negotiation_set('language', $negotiation);
}

/**
 * BatchAPI callback.
 *
 * @see capcorporate_install_additional_modules()
 */
function _capcorporate_flush_caches($operation, &$context) {
  $context['message'] = t('@operation', array('@operation' => $operation));
  drupal_flush_all_caches();
}
